/********************************************************************
	created:	2013/04/02
	author:		DRed(龙涛)
	purpose:	设计模式类
*********************************************************************/

class Singleton 
{  
	// 其它成员  
public:  
	static Singleton * GetInstance(){return m_pInstance;}
private:  
	Singleton();  
	static Singleton *m_pInstance;  
} 